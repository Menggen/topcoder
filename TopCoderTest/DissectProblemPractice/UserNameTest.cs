﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TopCoder.DissectProblemPractice;

namespace TopCoder.Test.DissectProblemPractice
{
    // [TestClass]
    public class UserNameTest
    {
        [TestMethod]
        public void Test_TygerTygerIsAvailable()
        {
            UserName problem = new UserName();
            String[] existingNames = new String[] { "MasterOfDisaster", "DingBat", "Orpheus", "WolfMan", "MrKnowItAll" };
            String newName = "TygerTyger";
            String expected = "TygerTyger";
            String actual = problem.newMember(existingNames, newName);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_TygerTygerAndTygerTyger1AreTaken()
        {
            UserName problem = new UserName();
            String[] existingNames = new String[] 
                {"MasterOfDisaster", "TygerTyger1", "DingBat", "Orpheus", 
                 "TygerTyger", "WolfMan", "MrKnowItAll"};
            String newName = "TygerTyger";
            String expected = "TygerTyger2";
            String actual = problem.newMember(existingNames, newName);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_BaseNameIsAvailable()
        {
            UserName problem = new UserName();
            String[] existingNames = new String[] 
                {"TygerTyger2000", "TygerTyger1", "MasterDisaster", "DingBat", 
                 "Orpheus", "WolfMan", "MrKnowItAll"};
            String newName = "TygerTyger";
            String expected = "TygerTyger";
            String actual = problem.newMember(existingNames, newName);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_CaseSensitive()
        {
            UserName problem = new UserName();
            String[] existingNames = new String[] 
                {"grokster2", "BrownEyedBoy", "Yoop", "BlueEyedGirl", 
                 "grokster", "Elemental", "NightShade", "Grokster1"};
            String newName = "grokster";
            String expected = "grokster1";
            String actual = problem.newMember(existingNames, newName);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_4()
        {
            UserName problem = new UserName();
            String[] existingNames = new String[] 
                {"Bart4", "Bart5", "Bart6", "Bart7", "Bart8", "Bart9", "Bart10",
                 "Lisa", "Marge", "Homer", "Bart", "Bart1", "Bart2", "Bart3",
                 "Bart11", "Bart12"};
            String newName = "Bart";
            String expected = "Bart13";
            String actual = problem.newMember(existingNames, newName);
            Assert.AreEqual(expected, actual);
        }
    }
}
