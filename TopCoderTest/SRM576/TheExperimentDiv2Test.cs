﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TopCoder.SRM576;

namespace TopCoder.Test.SRM576
{
    // [TestClass]
    public class TheExperimentDiv2Test
    {
        [TestMethod]
        public void Test_0()
        {
            TheExperimentDiv2 problem = new TheExperimentDiv2();
            int[] intensity = new int[] { 3, 4, 1, 1, 5, 6 };
            int L = 3;
            int[] leftEnd = new int[] { 3, 1, 0 };
            int[] expected = new int[] { 12, 5, 3 };
            int[] actual = problem.determineHumidity(intensity, L, leftEnd);
            AssertArray.AreEqual<int>(expected, actual);
        }

        [TestMethod]
        public void Test_1()
        {
            TheExperimentDiv2 problem = new TheExperimentDiv2();
            int[] intensity = new int[] { 5, 5 };
            int L = 2;
            int[] leftEnd = new int[] { 0, 0 };
            int[] expected = new int[] { 10, 0 };
            int[] actual = problem.determineHumidity(intensity, L, leftEnd);
            AssertArray.AreEqual<int>(expected, actual);
        }

        [TestMethod]
        public void Test_2()
        {
            TheExperimentDiv2 problem = new TheExperimentDiv2();
            int[] intensity = new int[] { 92, 11, 1000, 14, 3 };
            int L = 2;
            int[] leftEnd = new int[] { 0, 3 };
            int[] expected = new int[] { 103, 17 };
            int[] actual = problem.determineHumidity(intensity, L, leftEnd);
            AssertArray.AreEqual<int>(expected, actual);
        }

        [TestMethod]
        public void Test_3()
        {
            TheExperimentDiv2 problem = new TheExperimentDiv2();
            int[] intensity = new int[] { 2, 6, 15, 10, 8, 11, 99, 2, 4, 4, 4, 13 };
            int L = 4;
            int[] leftEnd = new int[] { 1, 7, 4, 5, 8, 0 };
            int[] expected = new int[] { 39, 14, 110, 0, 13, 2 };
            int[] actual = problem.determineHumidity(intensity, L, leftEnd);
            AssertArray.AreEqual<int>(expected, actual);
        }
    }
}
