﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TopCoder.DynamicProgrammingPractice;

namespace TopCoder.Test.DynamicProgrammingPractice
{
    // [TestClass]
    public class AvoidRoadsTest
    {
        [TestMethod]
        public void Test_0()
        {
            AvoidRoads problem = new AvoidRoads();
            int width = 6;
            int height = 6;
            String[] bad = { "0 0 0 1", "6 6 5 6" };
            long expected = 252;
            long actual = problem.numWays(width, height, bad);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_1()
        {
            AvoidRoads problem = new AvoidRoads();
            int width = 1;
            int height = 1;
            String[] bad = { };
            long expected = 2;
            long actual = problem.numWays(width, height, bad);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_2()
        {
            AvoidRoads problem = new AvoidRoads();
            int width = 35;
            int height = 31;
            String[] bad = { };
            long expected = 6406484391866534976;
            long actual = problem.numWays(width, height, bad);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_3()
        {
            AvoidRoads problem = new AvoidRoads();
            int width = 2;
            int height = 2;
            String[] bad = { "0 0 1 0", "1 2 2 2", "1 1 2 1" };
            long expected = 0;
            long actual = problem.numWays(width, height, bad);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_4()
        {
            AvoidRoads problem = new AvoidRoads();
            int width = 100;
            int height = 100;
            String[] bad = { "0 0 0 1", "0 0 1 0", "0 1 0 0" };
            long expected = 0;
            long actual = problem.numWays(width, height, bad);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_5()
        {
            AvoidRoads problem = new AvoidRoads();
            int width = 19;
            int height = 100;
            String[] bad = { "1 3 0 3", "0 4 0 3", "6 1 7 1", "4 7 4 8", "2 6 3 6", "0 6 0 7", "9 3 10 3", "2 4 2 3", "2 3 2 4", "7 6 7 7", "6 3 6 4", "8 7 8 8", "1 4 1 5", "0 2 1 2", "4 3 5 3", "2 9 2 10", "1 1 1 2", "9 5 8 5", "2 7 2 8", "6 0 7 0", "6 8 7 8", "2 2 2 1", "2 9 1 9", "5 8 5 9", "1 6 2 6", "9 4 8 4", "4 1 3 1", "7 5 8 5", "5 0 5 1", "3 6 2 6", "7 9 8 9", "6 8 7 8", "4 2 4 3", "5 0 4 0", "7 2 7 3", "4 4 5 4", "8 9 7 9", "5 3 6 3", "3 7 3 6", "0 8 1 8", "7 5 7 6", "9 3 10 3", "9 6 10 6", "1 9 1 10", "7 3 8 3", "4 6 4 7", "3 6 4 6", "3 1 3 0", "6 1 6 0", "8 3 7 3" };
            long expected = 1166309411843295530;
            long actual = problem.numWays(width, height, bad);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_6()
        {
            AvoidRoads problem = new AvoidRoads();
            int width = 9;
            int height = 100;
            String[] bad = { "0 2 0 3", "1 2 1 3", "2 2 2 3", "3 2 3 3", "4 2 4 3", "5 2 5 3", "6 2 6 3", "7 2 7 3", "8 2 8 3", "9 2 9 3" };
            long expected = 0;
            long actual = problem.numWays(width, height, bad);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_7()
        {
            AvoidRoads problem = new AvoidRoads();
            int width = 2;
            int height = 2;
            String[] bad = { "1 2 2 2", "1 1 2 1" };
            long expected = 1;
            long actual = problem.numWays(width, height, bad);
            Assert.AreEqual(expected, actual);
        }
    }
}
