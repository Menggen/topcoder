﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TopCoder.GraphPractice;

namespace TopCoder.Test.GraphPractice
{
    // [TestClass]
    public class PathFindingTest
    {
        [TestMethod]
        public void Test_0()
        {
            PathFinding problem = new PathFinding();
            String[] board = new String[]
                {"....",
                 ".A..",
                 "..B.",
                 "...."};
            int expected = 2;
            int actual = problem.minTurns(board);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_1()
        {
            PathFinding problem = new PathFinding();
            String[] board = new String[]
                {"XXXXXXXXX",
                 "A...X...B",
                 "XXXXXXXXX"};
            int expected = -1;
            int actual = problem.minTurns(board);
            Assert.AreEqual(expected, actual);
        }
            
        [TestMethod]
        public void Test_2()
        {
            PathFinding problem = new PathFinding();
            String[] board = new String[]
                {"XXXXXXXXX",
                 "A.......B",
                 "XXXX.XXXX"};
            int expected = 8;
            int actual = problem.minTurns(board);
            Assert.AreEqual(expected, actual);
        }
    }
}
