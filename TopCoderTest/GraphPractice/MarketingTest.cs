﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TopCoder.GraphPractice;

namespace TopCoder.Test.GraphPractice
{
    // [TestClass]
    public class MarketingTest
    {
        [TestMethod]
        public void Test_0()
        {
            Marketing problem = new Marketing();
            String[] compete = { "1 4", "2", "3", "0", "" };
            long expected = 2;
            long actual = problem.howMany(compete);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_1()
        {
            Marketing problem = new Marketing();
            String[] compete = { "1", "2", "0" };
            long expected = -1;
            long actual = problem.howMany(compete);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_2()
        {
            Marketing problem = new Marketing();
            String[] compete = { "1", "2", "3", "0", "0 5", "1" };
            long expected = 2;
            long actual = problem.howMany(compete);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_3()
        {
            Marketing problem = new Marketing();
            String[] compete = 
                {
                    "","","","","","","","","","",
                    "","","","","","","","","","",
                    "","","","","","","","","",""
                };
            long expected = 1073741824;
            long actual = problem.howMany(compete);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_4()
        {
            Marketing problem = new Marketing();
            String[] compete = { "1", "2", "3", "0", "5", "6", "4" };
            long expected = -1;
            long actual = problem.howMany(compete);
            Assert.AreEqual(expected, actual);
        }
    }
}
