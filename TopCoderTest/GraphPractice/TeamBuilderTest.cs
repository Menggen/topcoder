﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TopCoder.GraphPractice;

namespace TopCoder.Test.GraphPractice
{
    // [TestClass]
    public class TeamBuilderTest
    {
        public TeamBuilderTest() { }

        public TestContext TestContext { get; set; }

        [TestMethod]
        public void Test_0()
        {
            TeamBuilder problem = new TeamBuilder();
            String[] paths = new String[] { "010", "000", "110" };
            int[] expected = new int[] { 1, 1 };
            int[] actual = problem.specialLocations(paths);
            AssertArray.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_1()
        {
            TeamBuilder problem = new TeamBuilder();
            String[] paths = new String[] { "0010", "1000", "1100", "1000" };
            int[] expected = new int[] { 1, 3 };
            int[] actual = problem.specialLocations(paths);
            AssertArray.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_2()
        {
            TeamBuilder problem = new TeamBuilder();
            String[] paths = new String[] { "01000", "00100", "00010", "00001", "10000" };
            int[] expected = new int[] { 5, 5 };
            int[] actual = problem.specialLocations(paths);
            AssertArray.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_3()
        {
            TeamBuilder problem = new TeamBuilder();
            String[] paths = new String[] { "0110000", "1000100", "0000001", "0010000", "0110000", "1000010", "0001000" };
            int[] expected = new int[] { 1, 3 };
            int[] actual = problem.specialLocations(paths);
            AssertArray.AreEqual(expected, actual);
        }
    }
}
