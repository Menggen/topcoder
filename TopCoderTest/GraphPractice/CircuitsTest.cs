﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TopCoder.GraphPractice;

namespace TopCoder.Test.GraphPractice
{
    // [TestClass]
    public class CircuitsTest
    {
        [TestMethod]
        public void Test_0()
        {
            Circuits problem = new Circuits();
            String[] connects = new String[] { "1 2", "2", "" };
            String[] costs = new String[] { "5 3", "7", "" };
            int expected = 12;
            int actual = problem.howLong(connects, costs);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_1()
        {
            Circuits problem = new Circuits();
            String[] connects = new String[] { "1 2 3 4 5", "2 3 4 5", "3 4 5", "4 5", "5", "" };
            String[] costs = new String[] { "2 2 2 2 2", "2 2 2 2", "2 2 2", "2 2", "2", "" };
            int expected = 10;
            int actual = problem.howLong(connects, costs);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_2()
        {
            Circuits problem = new Circuits();
            String[] connects = new String[] { "1", "2", "3", "", "5", "6", "7", "" };
            String[] costs = new String[] { "2", "2", "2", "", "3", "3", "3", "" };
            int expected = 9;
            int actual = problem.howLong(connects, costs);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_3()
        {
            Circuits problem = new Circuits();
            String[] connects = new String[] { "", "2 3 5", "4 5", "5 6", "7", "7 8", "8 9", "10", "10 11 12", "11", "12", "12", "" };
            String[] costs = new String[] { "", "3 2 9", "2 4", "6 9", "3", "1 2", "1 2", "5", "5 6 9", "2", "5", "3", "" };
            int expected = 22;
            int actual = problem.howLong(connects, costs);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_4()
        {
            Circuits problem = new Circuits();
            String[] connects = new String[] { "", "2 3", "3 4 5", "4 6", "5 6", "7", "5 7", "" };
            String[] costs = new String[] { "", "30 50", "19 6 40", "12 10", "35 23", "8", "11 20", "" };
            int expected = 105;
            int actual = problem.howLong(connects, costs);
            Assert.AreEqual(expected, actual);
        } 
    }
}
