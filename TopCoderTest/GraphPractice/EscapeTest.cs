﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TopCoder.GraphPractice;

namespace TopCoder.Test.GraphPractice
{
    // [TestClass]
    public class EscapeTest
    {
        [TestMethod]
        public void Test_0()
        {
            Escape problem = new Escape();
            String[] harmful = new String[] { };
            String[] deadly = new String[] { };
            int expected = 0;
            int actual = problem.lowest(harmful, deadly);
            Assert.AreEqual(expected, actual);
        }
        
        [TestMethod]
        public void Test_1()
        {
            Escape problem = new Escape();
            String[] harmful = new String[] { "500 0 0 500" };
            String[] deadly = new String[] { "0 0 0 0" };
            int expected = 1000;
            int actual = problem.lowest(harmful, deadly);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_2()
        {
            Escape problem = new Escape();
            String[] harmful = new String[] { "0 0 250 250", "250 250 500 500" };
            String[] deadly = new String[] { "0 251 249 500", "251 0 500 249" };
            int expected = 1000;
            int actual = problem.lowest(harmful, deadly);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_3()
        {
            Escape problem = new Escape();
            String[] harmful = new String[] { "0 0 250 250", "250 250 500 500" };
            String[] deadly = new String[] { "0 250 250 500", "250 0 500 250" };
            int expected = -1;
            int actual = problem.lowest(harmful, deadly);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_4()
        {
            Escape problem = new Escape();
            String[] harmful = new String[] 
                {"468 209 456 32",
                 "71 260 306 427",
                 "420 90 424 492",
                 "374 253 54 253",
                 "319 334 152 431",
                 "38 93 204 84",
                 "246 0 434 263",
                 "12 18 118 461",
                 "215 462 44 317",
                 "447 214 28 475",
                 "3 89 38 125",
                 "157 108 138 264",
                 "363 17 333 387",
                 "457 362 396 324",
                 "95 27 374 175",
                 "381 196 265 302",
                 "105 255 253 134",
                 "0 308 453 55",
                 "169 28 313 498",
                 "103 247 165 376",
                 "264 287 363 407",
                 "185 255 110 415",
                 "475 126 293 112",
                 "285 200 66 484",
                 "60 178 461 301",
                 "347 352 470 479",
                 "433 130 383 370",
                 "405 378 117 377",
                 "403 324 369 133",
                 "12 63 174 309",
                 "181 0 356 56",
                 "473 380 315 378"};
            String[] deadly = new String[] 
                {"250 384 355 234",
                 "28 155 470 4",
                 "333 405 12 456",
                 "329 221 239 215",
                 "334 20 429 338",
                 "85 42 188 388",
                 "219 187 12 111",
                 "467 453 358 133",
                 "472 172 257 288",
                 "412 246 431 86",
                 "335 22 448 47",
                 "150 14 149 11",
                 "224 136 466 328",
                 "369 209 184 262",
                 "274 488 425 195",
                 "55 82 279 253",
                 "153 201 65 228",
                 "208 230 132 223",
                 "369 305 397 267",
                 "200 145 98 198",
                 "422 67 252 479",
                 "231 252 401 190",
                 "312 20 0 350",
                 "406 72 207 294",
                 "488 329 338 326",
                 "117 264 497 447",
                 "491 341 139 438",
                 "40 413 329 290",
                 "148 245 53 386",
                 "147 70 186 131",
                 "300 407 71 183",
                 "300 186 251 198",
                 "178 67 487 77",
                 "98 158 55 433",
                 "167 231 253 90",
                 "268 406 81 271",
                 "312 161 387 153",
                 "33 442 25 412",
                 "56 69 177 428",
                 "5 92 61 247"};
            int expected = 254;
            int actual = problem.lowest(harmful, deadly);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_5()
        {
            Escape problem = new Escape();
            String[] harmful = new String[] { "172 234 426 284", "214 471 366 291", "320 408 76 446", "223 131 323 470", "415 231 341 6", "298 442 222 269", "133 200 146 383", "453 76 222 108", "269 106 390 459", "235 291 398 113", "350 140 294 359", "377 430 265 260", "388 436 336 87", "208 281 122 36", "32 139 291 144", "155 423 339 323", "24 245 63 371", "275 369 306 309", "461 18 109 421", "413 401 431 373", "16 493 367 326", "181 122 148 10", "399 130 253 315", "102 408 65 477", "402 374 391 123", "352 16 475 473", "116 422 373 163", "196 278 97 312", "402 213 482 72", "330 393 42 129", "410 207 417 262", "430 499 93 326", "43 181 200 206", "399 289 62 54", "375 397 63 364", "366 90 62 202", "153 94 236 232", "150 429 200 416", "185 428 184 146", "24 255 281 371", "473 42 483 98", "232 94 287 101", "118 154 95 430" };
            String[] deadly = new String[] { "325 44 463 317", "160 98 24 418", "49 145 14 281", "84 438 337 372", "89 288 27 443", "415 359 277 499", "139 431 337 125", "409 196 239 405", "368 48 212 222", "476 60 38 247", "500 337 352 461", "116 157 22 155", "322 336 281 413", "251 31 198 172", "95 243 405 94", "467 66 175 101", "425 348 74 56", "325 220 351 254", "314 98 149 63", "492 97 373 220", "225 470 165 33", "88 55 76 430", "102 412 351 54", "282 224 171 363", "431 295 350 422", "248 382 118 429", "50 371 137 255", "285 293 240 123", "44 420 238 393", "464 235 231 232", "157 360 488 369", "341 49 380 443", "329 239 194 416", "360 189 421 383", "41 117 186 62" };
            int expected = 0;
            int actual = problem.lowest(harmful, deadly);
            Assert.AreEqual(expected, actual);
        }        
    }
}
