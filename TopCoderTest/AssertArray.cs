﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TopCoder.Test
{
    class AssertArray
    {
        public static void AreEqual<T>(T[] expected, T[] actual)
        {
            Assert.IsNotNull(expected);
            Assert.IsNotNull(actual);
            Assert.AreEqual<int>(expected.Length, actual.Length);
            for (int i = 0; i < expected.Length; i++)
            {
                Assert.AreEqual<T>(expected[i], actual[i]);
            }
        }
    }
}
