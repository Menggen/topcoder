﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TopCoder.GeometryPractice;

namespace TopCoder.Test.GeometryPractice
{
    // [TestClass]
    public class TVTowerTest
    {
        
        [TestMethod]
        public void Test_0()
        {
            TVTower problem = new TVTower();
            int[] x = new int[] { 1, 0, -1, 0 };
            int[] y = new int[] { 0, 1, 0, -1 };
            double expected = 1.0;
            double actual = problem.minRadius(x, y);
            Assert.AreEqual(expected, actual, ProblemDelta);
        }

        [TestMethod]
        public void Test_1()
        {
            TVTower problem = new TVTower();
            int[] x = new int[] { 3 };
            int[] y = new int[] { 299 };
            double expected = 0.0;
            double actual = problem.minRadius(x, y);
            Assert.AreEqual(expected, actual, ProblemDelta);
        }

        [TestMethod]
        public void Test_2()
        {
            TVTower problem = new TVTower();
            int[] x = new int[] { 5, 3, -4, 2 };
            int[] y = new int[] { 0, 4, 3, 2 };
            double expected = 4.743416490252569;
            double actual = problem.minRadius(x, y);
            Assert.AreEqual(expected, actual, ProblemDelta);
        }

        private double ProblemDelta = 1.0E-9;
    }
}
