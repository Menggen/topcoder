﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TopCoder.GeometryPractice;

namespace TopCoder.Test.GeometryPractice
{
    // [TestClass]
    public class PointTest
    {
        [TestMethod]
        public void Test_Minus()
        {
            Point a = new Point(10, 10);
            Point b = new Point(3, 6);
            Point actual = a.Minus(b);
            Assert.AreEqual(7, actual.X);
            Assert.AreEqual(4, actual.Y);
        }

        [TestMethod]
        public void Test_Length()
        {
            {
                Point p = new Point(1, 0);
                double actual = p.Length();
                Assert.AreEqual(1.0, actual, 0.00000001);
            }

            {
                Point p = new Point(3, 4);
                double actual = p.Length();
                Assert.AreEqual(5.0, actual, 0.00000001);
            }
        }

        [TestMethod]
        public void Test_Distance()
        {
            {
                Point a = new Point(1, 0);
                Point b = new Point(1, 0);
                double actual = Point.Distance(a, b);
                Assert.AreEqual(0.0, actual, 0.00000001);
            }

            {
                Point a = new Point(3, 0);
                Point b = new Point(0, 4);
                double actual = Point.Distance(a, b);
                Assert.AreEqual(5.0, actual, 0.00000001);
            }

            // Distance(a, b) should be equal to Distance(b, a).
            {
                Point a = new Point(3, 0);
                Point b = new Point(0, 4);
                double actual = Point.Distance(b, a);
                Assert.AreEqual(5.0, actual, 0.00000001);
            }
        }

        [TestMethod]
        public void Test_Dot()
        {
            {
                Point a = new Point(3, 4);
                Point b = new Point(1, 2);
                int actual = Point.Dot(a, b);
                Assert.AreEqual(11, actual);
            }

            // Dot(a, b) should be equal to Dot(b, a).
            {
                Point a = new Point(3, 4);
                Point b = new Point(1, 2);
                int actual = Point.Dot(b, a);
                Assert.AreEqual(11, actual);
            }

            {
                Random generator = new Random();
                Point p = new Point(generator.Next(100), generator.Next(100));
                double actual = Point.Dot(p, p);
                double expected = p.Length() * p.Length();
                Assert.AreEqual(expected, actual, 0.00000001);
            }
        }

        [TestMethod]
        public void Test_Cross()
        {
            {
                Point a = new Point(3, 4);
                Point b = new Point(1, 2);
                int actual = Point.Cross(a, b);
                Assert.AreEqual(2, actual);
            }

            // Cross(a, b) should be equal to -Cross(b, a).
            {
                Point a = new Point(3, 4);
                Point b = new Point(1, 2);
                int actual = Point.Cross(b, a);
                Assert.AreEqual(-2, actual);
            }

            // Calculate the area spanned by vector a and vector b.
            {
                Point a = new Point(3, 0);
                Point b = new Point(0, 2);
                int actual = Math.Abs(Point.Cross(a, b));
                Assert.AreEqual(6, actual);
            }
        }

        [TestMethod]
        public void Test_LineDistance()
        {
            {
                Point a = new Point(-1, 0);
                Point b = new Point(2, 0);
                Point p = new Point(0, 8);
                double actual = Point.LineDistance(a, b, p, false);
                Assert.AreEqual(8.0, actual, 0.00000001);
            }

            {
                Point a = new Point(-1, 0);
                Point b = new Point(2, 0);
                Point p = new Point(0, 8);
                double actual = Point.LineDistance(a, b, p, true);
                Assert.AreEqual(8.0, actual, 0.00000001);
            }

            {
                Point a = new Point(-1, 0);
                Point b = new Point(2, 0);
                Point p = new Point(-10, 8);
                double actual = Point.LineDistance(a, b, p, false);
                Assert.AreEqual(8.0, actual, 0.00000001);
            }

            {
                Point a = new Point(-1, 0);
                Point b = new Point(2, 0);
                Point p = new Point(-10, 8);
                double expected = Point.Distance(p, a);
                double actual = Point.LineDistance(a, b, p, true);
                Assert.AreNotEqual(8.0, actual, 0.00000001);
                Assert.AreEqual(expected, actual, 0.00000001);
            }

            {
                Point a = new Point(-1, 0);
                Point b = new Point(2, 0);
                Point p = new Point(13, 8);
                double actual = Point.LineDistance(a, b, p, false);
                Assert.AreEqual(8.0, actual, 0.00000001);
            }

            {
                Point a = new Point(-1, 0);
                Point b = new Point(2, 0);
                Point p = new Point(13, 8);
                double expected = Point.Distance(p, b);
                double actual = Point.LineDistance(a, b, p, true);
                Assert.AreNotEqual(8.0, actual, 0.00000001);
                Assert.AreEqual(expected, actual, 0.00000001);
            }
        }

        [TestMethod]
        public void Test_AreLinesIntersected_AllLines()
        {
            {
                Point a = new Point(-1, 0);
                Point b = new Point(1, 0);
                Point c = new Point(0, -1);
                Point d = new Point(0, 1);
                bool actual = Point.AreLinesIntersected
                    (
                        a, b, Point.LineType.Line,
                        c, d, Point.LineType.Line
                    );
                Assert.AreEqual(true, actual);
            }

            // Two lines are parallel.
            {
                Point a = new Point(-1, 0);
                Point b = new Point(1, 0);
                Point c = new Point(-3, -1);
                Point d = new Point(-6, -1);
                bool actual = Point.AreLinesIntersected
                    (
                        a, b, Point.LineType.Line,
                        c, d, Point.LineType.Line
                    );
                Assert.AreEqual(false, actual);
            }

            // Two lines are identical.
            {
                Point a = new Point(-1, 0);
                Point b = new Point(1, 0);
                Point c = new Point(3, 0);
                Point d = new Point(5, 0);
                bool actual = Point.AreLinesIntersected
                    (
                        a, b, Point.LineType.Line,
                        c, d, Point.LineType.Line
                    );
                Assert.AreEqual(false, actual);
            }
        }

        [TestMethod]
        public void Test_AreLinesIntersected_SegmentAndRay()
        {
            {
                Point a = new Point(-1, 0);
                Point b = new Point(1, 0);
                Point c = new Point(0, 1);
                Point d = new Point(0, -1);
                bool actual = Point.AreLinesIntersected
                    (
                        a, b, Point.LineType.Segment,
                        c, d, Point.LineType.Ray
                    );
                Assert.AreEqual(true, actual);
            }

            {
                Point a = new Point(-1, 0);
                Point b = new Point(1, 0);
                Point c = new Point(0, -1);
                Point d = new Point(0, -2);
                bool actual = Point.AreLinesIntersected
                    (
                        a, b, Point.LineType.Segment,
                        c, d, Point.LineType.Ray
                    );
                Assert.AreEqual(false, actual);
            }

            {
                Point a = new Point(-1, 0);
                Point b = new Point(1, 0);
                Point c = new Point(1, 1);
                Point d = new Point(1, -1);
                bool actual = Point.AreLinesIntersected
                    (
                        a, b, Point.LineType.Segment,
                        c, d, Point.LineType.Ray
                    );
                Assert.AreEqual(true, actual);
            }

            {
                Point a = new Point(-1, 0);
                Point b = new Point(1, 0);
                Point c = new Point(3, 1);
                Point d = new Point(3, -1);
                bool actual = Point.AreLinesIntersected
                    (
                        a, b, Point.LineType.Segment,
                        c, d, Point.LineType.Ray
                    );
                Assert.AreEqual(false, actual);
            }
        }
    }
}
