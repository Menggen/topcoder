﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TopCoder.GeometryPractice;

namespace TopCoder.Test.GeometryPractice
{
    // [TestClass]
    public class SurveyorTest
    {
        [TestMethod]
        public void Test_Square()
        {
            Surveyor problem = new Surveyor();
            String direction = "NWWSE";
            int[] length = new int[] { 10, 3, 7, 10, 10 };
            int expected = 100;
            int actual = problem.area(direction, length);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_1()
        {
            Surveyor problem = new Surveyor();
            String direction = "NESWNWSW";
            int[] length = new int[] { 20, 200, 30, 100, 20, 30, 10, 70 };
            int expected = 4700;
            int actual = problem.area(direction, length);
            Assert.AreEqual(expected, actual);
        }
    }
}
