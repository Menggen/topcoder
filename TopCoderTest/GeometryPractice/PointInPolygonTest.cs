﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TopCoder.GeometryPractice;

namespace TopCoder.Test.GeometryPractice
{
    // [TestClass]
    public class PointInPolygonTest
    {
        [TestMethod]
        public void Test_SimpleExampleOfSquareOfSide10()
        {
            PointInPolygon problem = new PointInPolygon();
            String[] vertices = new String[]
                {"0 0",
                 "0 10",
                 "10 10",
                 "10 0"};
            int testPointX = 5;
            int testPointY = 5;
            String expected = "INTERIOR";
            String actual = problem.testPoint(vertices, testPointX, testPointY);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_OutsideSameSquare()
        {
            PointInPolygon problem = new PointInPolygon();
            String[] vertices = new String[]
                {"0 0",
                 "0 10",
                 "10 10",
                 "10 0"};
            int testPointX = 10;
            int testPointY = 15;
            String expected = "EXTERIOR";
            String actual = problem.testPoint(vertices, testPointX, testPointY);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_OnEdgeOfSquare()
        {
            PointInPolygon problem = new PointInPolygon();
            String[] vertices = new String[]
                {"0 0",
                 "0 10",
                 "10 10",
                 "10 0"};
            int testPointX = 5;
            int testPointY = 10;
            String expected = "BOUNDARY";
            String actual = problem.testPoint(vertices, testPointX, testPointY);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_MoreComplexGeometry()
        {
            PointInPolygon problem = new PointInPolygon();
            String[] vertices = new String[]
                {"-100 -90", "-100 100","100 100", "100 -100",
                 "-120 -100","-120 100","-130 100","-130 -110",
                 "110 -110", "110 110", "-110 110","-110 -90"};
            int testPointX = 0;
            int testPointY = 0;
            String expected = "EXTERIOR";
            String actual = problem.testPoint(vertices, testPointX, testPointY);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_4()
        {
            PointInPolygon problem = new PointInPolygon();
            String[] vertices = new String[]
                {"0 0","0 1000","1000 1000","1000 800",
                 "200 800","200 600","600 600","600 400",
                 "200 400","200 200","1000 200","1000 0"};
            int testPointX = 100;
            int testPointY = 500;
            String expected = "INTERIOR";
            String actual = problem.testPoint(vertices, testPointX, testPointY);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_5()
        {
            PointInPolygon problem = new PointInPolygon();
            String[] vertices = new String[]
                {"0 1000","1000 1000","1000 800",
                 "200 800","200 600","600 600","600 400",
                 "200 400","200 200","1000 200","1000 0","0 0"};
            int testPointX = 322;
            int testPointY = 333;
            String expected = "EXTERIOR";
            String actual = problem.testPoint(vertices, testPointX, testPointY);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_6()
        {
            PointInPolygon problem = new PointInPolygon();
            String[] vertices = new String[]
                {"500 0","500 100","400 100","400 200","300 200",
                 "300 300","200 300","200 400","100 400","100 500",
                 "0 500","0 400","-100 400","-100 300","-200 300",
                  "-200 200","-300 200","-300 100","-400 100","-400 0",
                  "-500 0","-500 -100","-400 -100","-400 -200","-300 -200",
                  "-300 -300","-200 -300","-200 -400","-100 -400","-100 -500",
                  "0 -500","0 -400","100 -400","100 -300","200 -300",
                  "200 -200","300 -200","300 -100","400 -100","400 0"};
            int testPointX = 200;
            int testPointY = 200;
            String expected = "INTERIOR";
            String actual = problem.testPoint(vertices, testPointX, testPointY);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_7()
        {
            PointInPolygon problem = new PointInPolygon();
            String[] vertices = new String[]
                {"1 0","2 0","2 1","3 1","3 0","4 0","4 -1","5 -1","5 0",
                 "6 0","6 2","0 2","0 3","-1 3","-1 4","0 4","0 6","1 6",
                 "1 7","0 7","0 8","-2 8","-2 2","-8 2","-8 0","-7 0",
                 "-7 -1","-6 -1","-6 0","-4 0","-4 1","-3 1","-3 0",
                 "-2 0","-2 -6","0 -6","0 -5","1 -5","1 -4","0 -4",
                 "0 -3","-1 -3","-1 -2","0 -2","0 -1","1 -1"};
            int testPointX = 0;
            int testPointY = 0;
            String expected = "INTERIOR";
            String actual = problem.testPoint(vertices, testPointX, testPointY);
            Assert.AreEqual(expected, actual);
        }
    }
}
