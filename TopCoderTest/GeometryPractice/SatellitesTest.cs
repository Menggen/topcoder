﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TopCoder.GeometryPractice;

namespace TopCoder.Test.GeometryPractice
{
    // [TestClass]
    public class SatellitesTest
    {
        [TestMethod]
        public void Test_SameLatitudeAndLongitudeAsRocket()
        {
            Satellites problem = new Satellites();
            String[] rockets = new String[] { "+0000.000 -0000.000" };
            String[] satellites = new String[] 
                { "+0000.000 -0000.000 +0200.000",
                  "+0000.000 -0000.000 +0400.000",
                  "+0000.000 -0000.000 +1200.000"};
            int[] expected = new int[] { 0 };
            int[] actual = problem.detectable(rockets, satellites);
            AssertArray.AreEqual<int>(expected, actual);
        }

        [TestMethod]
        public void Test_1()
        {
            Satellites problem = new Satellites();
            String[] rockets = new String[] { "-0050.000 +0045.000", "+0040.000 -0135.000" };
            String[] satellites = new String[] 
                {"+0090.000 +0000.000 +1200.000",
                 "-0090.000 +0000.000 +1200.000",
                 "+0000.000 +0000.000 +1200.000",
                 "+0000.000 -0090.000 +1200.000",
                 "+0000.000 +0180.000 +1200.000",
                 "-0000.000 -0045.000 +1200.000",
                 "-0000.000 -0135.000 +1000.000",
                 "-0011.000 -0136.000 +1086.828"};
            int[] expected = new int[] { 1 };
            int[] actual = problem.detectable(rockets, satellites);
            AssertArray.AreEqual<int>(expected, actual);
        }

        [TestMethod]
        public void Test_2()
        {
            Satellites problem = new Satellites();
            String[] rockets = new String[]
                {"+0037.431 -0143.361",
                 "+0037.443 -0143.375",
                 "+0037.413 -0143.364"};
            String[] satellites = new String[] 
                {"+0037.470 -0143.316 +0513.143",
                 "+0037.443 -0143.388 +0342.159",
                 "+0037.434 -0143.361 +1034.123"};
            int[] expected = new int[] { 0, 1, 2 };
            int[] actual = problem.detectable(rockets, satellites);
            AssertArray.AreEqual<int>(expected, actual);
        }
    }
}
