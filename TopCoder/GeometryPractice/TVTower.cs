﻿using System;
using System.Collections.Generic;

namespace TopCoder.GeometryPractice
{
    // http://community.topcoder.com/stat?c=problem_statement&pm=2260&rd=4735
    public class TVTower
    {
        public double minRadius(int[] x, int[] y)
        {
            this.X = x;
            this.Y = y;
            this.currMinRadius = Double.MaxValue;

            int N = x.Length;
            if (N == 1)
            {
                return 0.0;
            }

            for (int i = 0; i < N; i++)
            {
                for (int j = i + 1; j < N; j++)
                {
                    double centerX = (x[i] + x[j]) / 2.0;
                    double centerY = (y[i] + y[j]) / 2.0;
                    UpdateCurrMinRadius(centerX, centerY);
                    for (int k = j + 1; k < N; k++)
                    {
                        double[] center = GetCenter(X[i], Y[i], X[j], Y[j], X[k], Y[k]);
                        UpdateCurrMinRadius(center[0], center[1]);
                    }
                }
            }

            return this.currMinRadius;
        }

        private void UpdateCurrMinRadius(double centerX, double centerY)
        {
            double maxRadius = 0.0;
            
            int N = X.Length;
            for (int i = 0; i < N; i++)
            {
                double distance = GetDistance(centerX, centerY, X[i], Y[i]);
                if (distance > maxRadius)
                {
                    maxRadius = distance;
                }
            }

            if (maxRadius < currMinRadius)
            {
                currMinRadius = maxRadius;
            }
        }

        private double GetDistance(double x1, double y1, double x2, double y2)
        {
            double dx = x2 - x1;
            double dy = y2 - y1;
            return Math.Sqrt(dx * dx + dy * dy);
        }

        private double[] GetCenter
            (
                double x1,
                double y1,
                double x2,
                double y2,
                double x3,
                double y3
            )
        {
            double a = x2 - x1;
            double b = y2 - y1;
            double c = (x2 * x2 - x1 * x1) / 2.0 + (y2 * y2 - y1 * y1) / 2.0;
            double d = x3 - x2;
            double e = y3 - y2;
            double f = (x3 * x3 - x2 * x2) / 2.0 + (y3 * y3 - y2 * y2) / 2.0;

            double det = a * e - b * d;
            double centerX = (c * e - b * f) / det;
            double centerY = (a * f - c * d) / det;

            return new double[] { centerX, centerY };
        }

        private int[] X { get; set; }

        private int[] Y { get; set; }

        private double currMinRadius = Double.MaxValue;
    }
}
