﻿using System;
using System.Collections.Generic;

namespace TopCoder.GeometryPractice
{
    // http://community.topcoder.com/tc?module=ProblemDetail&rd=5000&pm=1359
    public class Surveyor
    {
        public int area(String direction, int[] length)
        {
            int N = direction.Length;

            // Represent the polygon in the Cartesian coordinates.
            List<int> x = new List<int>();
            List<int> y = new List<int>();
            int currX = 0;
            int currY = 0;
            for (int i = 0; i < N; i++)
            {
                if (direction[i] == 'N')
                {
                    currY += length[i];
                }
                else if (direction[i] == 'S')
                {
                    currY -= length[i];
                }
                else if (direction[i] == 'E')
                {
                    currX += length[i];
                }
                else if (direction[i] == 'W')
                {
                    currX -= length[i];
                }
                x.Add(currX);
                y.Add(currY);
            }

            // Calculate the polygon area by triangulating the polygon.
            int doubleArea = 0;
            for (int i = 1; i + 1 < N; i++)
            {
                int x1 = x[i] - x[0];
                int y1 = y[i] - y[0];
                int x2 = x[i + 1] - x[0];
                int y2 = y[i + 1] - y[0];
                doubleArea += (x1 * y2 - x2 * y1);
            }

            return Math.Abs(doubleArea / 2);
        }
    }
}
