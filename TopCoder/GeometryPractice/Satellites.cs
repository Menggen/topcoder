﻿using System;
using System.Collections.Generic;

namespace TopCoder.GeometryPractice
{
    // http://community.topcoder.com/stat?c=problem_statement&pm=1812&rd=4720
    public class Satellites
    {
        public int[] detectable(String[] rockets, String[] satellites)
        {
            List<Point3D> satellitePoints = new List<Point3D>();
            for (int i = 0; i < satellites.Length; i++)
            {
                satellitePoints.Add(GetSatellitePosition(satellites[i]));
            }

            List<int> detectableRocketList = new List<int>();
            Point3D O = new Point3D(0, 0, 0);
            for (int i = 0; i < rockets.Length; i++)
            {
                Point3D R = GetRocketPosition(rockets[i]);
                int seenCount = 0;
                foreach (Point3D S in satellitePoints)
                {
                    double distance = GetLineDistance(R, S, O, true);
                    if (distance > EarthRadius)
                    {
                        seenCount++;
                    }
                }
                if (seenCount >= 3)
                {
                    detectableRocketList.Add(i);   
                }
            }

            detectableRocketList.Sort();

            return detectableRocketList.ToArray();
        }

        private Point3D GetRocketPosition(String rocketString)
        {
            double latitude = Double.Parse(rocketString.Substring(0, 9));
            double longitude = Double.Parse(rocketString.Substring(10));
            double altitude = EarthRadius + 400.0;
            
            double cosAltitude = Math.Cos(latitude / 180 * Math.PI) * altitude;
            double x = Math.Sin(longitude / 180 * Math.PI) * cosAltitude;
            double y = Math.Cos(longitude / 180 * Math.PI) * cosAltitude;
            double z = Math.Sin(latitude / 180 * Math.PI) * altitude;

            return new Point3D(x, y, z);
        }

        private Point3D GetSatellitePosition(String satelliteString)
        {
            double latitude = Double.Parse(satelliteString.Substring(0, 9));
            double longitude = Double.Parse(satelliteString.Substring(10, 9));
            double altitude = EarthRadius + Double.Parse(satelliteString.Substring(20));

            double cosAltitude = Math.Cos(latitude / 180 * Math.PI) * altitude;
            double x = Math.Sin(longitude / 180 * Math.PI) * cosAltitude;
            double y = Math.Cos(longitude / 180 * Math.PI) * cosAltitude;
            double z = Math.Sin(latitude / 180 * Math.PI) * altitude;

            return new Point3D(x, y, z);
        }

        private double GetLineDistance
            (
                Point3D lineStartPoint,
                Point3D lineEndPoint,
                Point3D point,
                bool isSegment
            )
        {
            Point3D lineVector = lineEndPoint.Minus(lineStartPoint);

            if (lineVector.Length() == 0)
            {
                return point.Minus(lineStartPoint).Length();
            }

            Point3D pointVector = point.Minus(lineStartPoint);
            double distance = Point3D.Cross(lineVector, pointVector).Length() / lineVector.Length();
            if (isSegment)
            {
                {
                    Point3D testVector = point.Minus(lineEndPoint);
                    double dot = Point3D.Dot(testVector, lineVector);
                    if (dot > 0.0)
                    {
                        return testVector.Length();
                    }
                }
                {
                    Point3D testVector = lineStartPoint.Minus(point);
                    double dot = Point3D.Dot(testVector, lineVector);
                    if (dot > 0)
                    {
                        return testVector.Length();
                    }
                }
            }
            return distance;
        }

        private class Point3D
        {
            public double X;
            public double Y;
            public double Z;

            public Point3D() { }

            public Point3D(double x, double y, double z)
            {
                this.X = x;
                this.Y = y;
                this.Z = z;
            }

            public Point3D Minus(Point3D another)
            {
                return new Point3D(X - another.X, Y - another.Y, Z - another.Z);
            }

            public double Length()
            {
                return Math.Sqrt(X * X + Y * Y + Z * Z);
            }

            public static double Dot(Point3D a, Point3D b)
            {
                return a.X * b.X + a.Y * b.Y + a.Z * b.Z;
            }

            public static Point3D Cross(Point3D a, Point3D b)
            {
                double x = a.Y * b.Z - b.Y * a.Z;
                double y = a.Z * b.X - b.Z * a.X;
                double z = a.X * b.Y - b.X * a.Y;
                return new Point3D(x, y, z);
            }
        }

        // The Earth is a sphere with a diameter of 12800 kilometers.
        private double EarthRadius = 12800.0 / 2.0;
    }
}
