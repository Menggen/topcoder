﻿using System;
using System.Collections.Generic;

namespace TopCoder.GeometryPractice
{
    // http://community.topcoder.com/tc?module=ProblemDetail&rd=4635&pm=1660
    public class ConvexPolygon
    {
        public double findArea(int[] x, int[] y)
        {
            int doubleArea = 0;

            int N = x.Length;

            for (int i = 1; i + 1 < N; i++)
            {
                int x1 = x[i] - x[0];
                int y1 = y[i] - y[0];
                int x2 = x[i + 1] - x[0];
                int y2 = y[i + 1] - y[0];
                doubleArea += (x1 * y2 - x2 * y1);
            }

            return Math.Abs(doubleArea / 2.0);
        }
    }
}
