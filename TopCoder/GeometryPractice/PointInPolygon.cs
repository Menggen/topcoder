﻿using System;
using System.Collections.Generic;

namespace TopCoder.GeometryPractice
{
    // http://community.topcoder.com/tc?module=Static&d1=tutorials&d2=geometry3#satellites
    // http://community.topcoder.com/stat?c=problem_statement&pm=2384&rd=4755
    public class PointInPolygon
    {
        public String testPoint(String[] vertices, int testPointX, int testPointY)
        {
            Point testPoint = new Point(testPointX, testPointY);
            Point referencePoint = new Point(testPointX + 1, testPointY + 1);
            int N = vertices.Length;

            int intersectedCount = 0;

            for (int i = 0; i < N; i++)
            {
                Point startPoint = GetPoint(vertices[i]);
                Point endPoint = GetPoint(vertices[(i + 1) % N]);
                
                double distance = Point.LineDistance(startPoint, endPoint, testPoint, true);
                if (distance < 0.00000001)
                {
                    return "BOUNDARY";
                }

                bool isIntersected = Point.AreLinesIntersected
                    (
                        startPoint,
                        endPoint,
                        Point.LineType.OpenClosedSegment,
                        testPoint,
                        referencePoint,
                        Point.LineType.Ray
                    );
                if (isIntersected)
                {
                    intersectedCount++;
                }
            }

            if (intersectedCount % 2 == 0)
            {
                return "EXTERIOR";
            }
            else
            {
                return "INTERIOR";
            }
        }

        private Point GetPoint(String pointString)
        {
            String[] coordinates = pointString.Split();
            int x = Int32.Parse(coordinates[0]);
            int y = Int32.Parse(coordinates[1]);
            return new Point(x, y);
        }
    }
}
