﻿using System;
using System.Collections.Generic;

namespace TopCoder.GeometryPractice
{
    public class Point
    {
        public int X { get; set; }

        public int Y { get; set; }

        public Point() { }

        public Point(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public Point Minus(Point another)
        {
            return new Point(this.X - another.X, this.Y - another.Y);
        }

        public double Length()
        {
            return Math.Sqrt(X * X + Y * Y);
        }

        public static double Distance(Point a, Point b)
        {
            return a.Minus(b).Length();
        }

        public static int Dot(Point a, Point b)
        {
            return a.X * b.X + a.Y * b.Y;
        }

        public static int Cross(Point a, Point b)
        {
            return a.X * b.Y - a.Y * b.X;
        }

        public static double LineDistance
            (
                Point lineStartPoint,
                Point lineEndPoint,
                Point point,
                bool isSegment
            )
        {
            Point lineVector = lineEndPoint.Minus(lineStartPoint);
            Point pointVector = point.Minus(lineStartPoint);
            double distance = Cross(lineVector, pointVector) / lineVector.Length();
            if (isSegment)
            {
                {
                    Point testVector = point.Minus(lineEndPoint);
                    int dot = Dot(testVector, lineVector);
                    if (dot > 0)
                    {
                        return testVector.Length();
                    }
                }
                {
                    Point testVector = lineStartPoint.Minus(point);
                    int dot = Dot(testVector, lineVector);
                    if (dot > 0)
                    {
                        return testVector.Length();
                    }
                }
            }
            return Math.Abs(distance);
        }

        public static bool AreLinesIntersected
            (
                Point lineAStartPoint,
                Point lineAEndPoint,
                LineType lineAType,
                Point lineBStartPoint,
                Point lineBEndPoint,
                LineType lineBType
            )
        {
            // Line A representation: ax + by = c
            int a = lineAEndPoint.Y - lineAStartPoint.Y;
            int b = lineAStartPoint.X - lineAEndPoint.X;
            int c = lineAStartPoint.X * lineAEndPoint.Y
                    - lineAEndPoint.X * lineAStartPoint.Y;

            // Line B representation: dx + ey = f
            int d = lineBEndPoint.Y - lineBStartPoint.Y;
            int e = lineBStartPoint.X - lineBEndPoint.X;
            int f = lineBStartPoint.X * lineBEndPoint.Y
                    - lineBEndPoint.X * lineBStartPoint.Y;

            int det = a * e - b * d;

            // Design choice: If two lines are identical, we regards that
            // two lines are not intersected.
            if (det == 0)
            {
                return false;
            }
            else
            {
                double x = (double)(c * e - b * f) / (double)det;
                double y = (double)(a * f - c * d) / (double)det;
                if (lineAType == LineType.Segment)
                {
                    if (!IsInSegment(lineAStartPoint.X, lineAEndPoint.X, x))
                    {
                        return false;
                    }
                    if (!IsInSegment(lineAStartPoint.Y, lineAEndPoint.Y, y))
                    {
                        return false;
                    }
                }
                else if (lineAType == LineType.OpenClosedSegment)
                {
                    if (!IsInOpenClosedSegment(lineAStartPoint.X, lineAEndPoint.X, x))
                    {
                        return false;
                    }
                    if (!IsInOpenClosedSegment(lineAStartPoint.Y, lineAEndPoint.Y, y))
                    {
                        return false;
                    }
                }
                else if (lineAType == LineType.Ray)
                {
                    if (!IsInRay(lineAStartPoint.X, lineAEndPoint.X, x))
                    {
                        return false;
                    }
                    if (!IsInRay(lineAStartPoint.Y, lineAEndPoint.Y, y))
                    {
                        return false;
                    }
                }

                if (lineBType == LineType.Segment)
                {
                    if (!IsInSegment(lineBStartPoint.X, lineBEndPoint.X, x))
                    {
                        return false;
                    }
                    if (!IsInSegment(lineBStartPoint.Y, lineBEndPoint.Y, y))
                    {
                        return false;
                    }
                }
                else if (lineBType == LineType.OpenClosedSegment)
                {
                    if (!IsInOpenClosedSegment(lineBStartPoint.X, lineBEndPoint.X, x))
                    {
                        return false;
                    }
                    if (!IsInOpenClosedSegment(lineBStartPoint.Y, lineBEndPoint.Y, y))
                    {
                        return false;
                    }
                }
                else if (lineBType == LineType.Ray)
                {
                    if (!IsInRay(lineBStartPoint.X, lineBEndPoint.X, x))
                    {
                        return false;
                    }
                    if (!IsInRay(lineBStartPoint.Y, lineBEndPoint.Y, y))
                    {
                        return false;
                    }
                }

                return true;
            }

            throw new NotImplementedException();
        }

        public enum LineType
        {
            Line,
            Segment,
            OpenClosedSegment,
            Ray
        }

        private static bool IsInSegment(int startValue, int endValue, double testValue)
        {
            if (startValue < endValue)
            {
                return testValue >= startValue && testValue <= endValue;
            }
            else if (startValue > endValue)
            {
                return testValue <= startValue && testValue >= endValue;
            }
            else
            {
                return Math.Abs(testValue - startValue) < 0.00000001;
            }
        }

        private static bool IsInOpenClosedSegment(int startValue, int endValue, double testValue)
        {
            if (startValue < endValue)
            {
                return testValue > startValue && testValue <= endValue;
            }
            else if (startValue > endValue)
            {
                return testValue < startValue && testValue >= endValue;
            }
            else
            {
                return true;
            }
        }

        private static bool IsInRay(int startValue, int endValue, double testValue)
        {
            if (startValue < endValue)
            {
                return testValue >= startValue;
            }
            else if (startValue > endValue)
            {
                return testValue <= startValue;
            }
            else
            {
                return true;
            }
        }
    }
}
