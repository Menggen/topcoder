﻿using System;
using System.Collections.Generic;

namespace TopCoder.DissectProblemPractice
{
    public class UserName
    {
        public String newMember(String[] existingNames, String newName)
        {
            var map = new Dictionary<string, HashSet<int>>();
            foreach (String name in existingNames)
            { 
                int index = GetNumberAttachedIndex(name);
                if (index == -1)
                {
                    if (!map.ContainsKey(name))
                    {
                        map.Add(name, new HashSet<int>());
                    }
                    map[name].Add(0);
                }
                else
                {
                    String nameWithoutNumber = name.Substring(0, index);
                    int number = Int32.Parse(name.Substring(index));

                    if (!map.ContainsKey(nameWithoutNumber))
                    {
                        map.Add(nameWithoutNumber, new HashSet<int>());
                    }
                    map[nameWithoutNumber].Add(number);
                }
            }

            if (map.ContainsKey(newName))
            {
                HashSet<int> numberSet = map[newName];
                for (int i = 0; i <= numberSet.Count; i++)
                {
                    if (!numberSet.Contains(i))
                    {
                        if (i == 0)
                        {
                            return newName;
                        }
                        else
                        {
                            return String.Format("{0}{1}", newName, i);
                        }
                    }
                }
            }

            return newName;
        }

        private int GetNumberAttachedIndex(String name)
        {
            for (int i = 0; i < name.Length; i++)
            {
                if (name[i] >= '1' && name[i] <= '9')
                {
                    return i;
                }
            }
            return -1;
        }
    }
}
