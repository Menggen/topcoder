﻿using System;
using System.Collections.Generic;

namespace TopCoder.GraphPractice
{
    public class KiloManX
    {
        public int leastShots(String[] damageChart, int[] bossHealth)
        {
            int bossCount = damageChart.Length;
            bool[] visited = new bool[1 << bossCount];
            MinHeap<Node> priorityQueue = new MinHeap<Node>();

            priorityQueue.Push(new Node(0, 0));

            while (priorityQueue.Count > 0)
            {
                Node top = priorityQueue.Pop();

                if (visited[top.WeaponSet])
                {
                    continue;
                }
                visited[top.WeaponSet] = true;

                if (top.WeaponSet == (1 << bossCount) - 1)
                {
                    return top.Shots;
                }

                for (int i = 0; i < bossCount; i++)
                {
                    if (((top.WeaponSet >> i) & 1) == 1)
                    {
                        continue;
                    }

                    int bestShots = bossHealth[i];
                    for (int j = 0; j < bossCount; j++)
                    {
                        if (i == j)
                        {
                            continue;
                        }
                        if (((top.WeaponSet >> j) & 1) == 1 && damageChart[j][i] != '0')
                        {
                            int shots = bossHealth[i] / (damageChart[j][i] - '0');
                            if (bossHealth[i] % (damageChart[j][i] - '0') != 0)
                            {
                                shots++;
                            }
                            if (shots < bestShots)
                            {
                                bestShots = shots;
                            }
                        }
                    }
                    Node next = new Node(top.WeaponSet | (1 << i), top.Shots + bestShots);
                    priorityQueue.Push(next);
                }
            }

            return -1;
        }

        private class Node : IComparable
        {
            // There are 15 different weapons.  
            // We encode weapons in the integers between 0 to 2^15 - 1 and 
            // then operate weapons by bitwise operations.
            public int WeaponSet { get; set; }

            public int Shots { get; set; }

            int IComparable.CompareTo(object obj)
            {
                Node another = (Node)obj;
                if (this.Shots > another.Shots)
                {
                    return 1;
                }
                else if (this.Shots < another.Shots)
                {
                    return -1;
                }
                return 0;
            }

            public Node() { }

            public Node(int weaponSet, int shots)
            {
                WeaponSet = weaponSet;
                Shots = shots;
            }
        }
    }
}
