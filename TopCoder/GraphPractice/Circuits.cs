﻿using System;
using System.Collections.Generic;

namespace TopCoder.GraphPractice
{
    // http://community.topcoder.com/stat?c=problem_statement&pm=1593&rd=4494
    public class Circuits
    {
        public int howLong(String[] connects, String[] costs)
        {
            int[,] graph = GetGraph(connects, costs);
            bool[] visitedMap = new bool[connects.Length];
            int[] distances = new int[connects.Length];

            for (int i = 0; i < connects.Length; i++)
            {
                if (!visitedMap[i])
                {
                    UpdateDistanceStartHere
                        (
                            i, 
                            ref visitedMap, 
                            ref graph, 
                            ref distances
                        );
                }
            }

            return Max(distances);
        }

        private void UpdateDistanceStartHere
            (
                int whichVertex, 
                ref bool[] visitedMap, 
                ref int[,] graph,
                ref int[] distances
            )
        {
            Stack<int> stack = new Stack<int>();
            stack.Push(whichVertex);

            while (stack.Count > 0)
            {
                int top = stack.Pop();
                visitedMap[top] = true;

                for (int j = 0; j < distances.Length; j++)
                {
                    if (graph[top, j] > 0)
                    {
                        int distanceStartFromTop = distances[top] + graph[top, j];
                        if (distanceStartFromTop > distances[j])
                        {
                            distances[j] = distanceStartFromTop;
                        }
                        stack.Push(j);
                    }
                }
            }
        }

        private int[,] GetGraph(String[] connects, String[] costs)
        {
            int vertexCount = connects.Length;
            int[,] graph = new int[vertexCount, vertexCount];

            for (int i = 0; i < vertexCount; i++)
            {
                for (int j = 0; j < vertexCount; j++)
                {
                    if (i != j)
                    {
                        graph[i, j] = -1;
                    }
                }
            }

            for (int i = 0; i < vertexCount; i++)
            {
                if (String.IsNullOrEmpty(connects[i]))
                {
                    continue;
                }

                String[] adjacentVertices = connects[i].Split();
                String[] adjacentCosts = costs[i].Split();
                for (int j = 0; j < adjacentVertices.Length; j++)
                { 
                    int whichVertex = Int32.Parse(adjacentVertices[j]);
                    int whichCost = Int32.Parse(adjacentCosts[j]);
                    graph[i, whichVertex] = whichCost;
                }
            }

            return graph;
        }

        private int Max(int[] array)
        {
            int max = array[0];

            foreach (int entry in array)
            {
                if (entry > max)
                {
                    max = entry;
                }
            }

            return max;
        }
    }
}
