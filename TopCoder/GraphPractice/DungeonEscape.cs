﻿using System;
using System.Collections.Generic;

namespace TopCoder.GraphPractice
{
    // http://community.topcoder.com/stat?c=problem_statement&pm=2449&rd=5073
    public class DungeonEscape
    {
        public int exitTime
            (
                String[] up,
                String[] down,
                String[] east,
                String[] west,
                int startLevel,
                int startEasting
            )
        {
            int width = up[0].Length;
            int height = up.Length;
            bool[,] visited = new bool[width, height];

            Node startNode = new Node(startEasting, startLevel, 0);

            MinHeap<Node> priorityQueue = new MinHeap<Node>();

            priorityQueue.Push(startNode);

            while (priorityQueue.Count > 0)
            {
                Node top = priorityQueue.Pop();

                if (top.X < 0 || top.X >= width || top.Y >= height)
                {
                    continue;
                }

                // Reach the surface
                if (top.Y < 0)
                {
                    return top.Times;
                }

                if (visited[top.X, top.Y])
                {
                    continue;
                }
                visited[top.X, top.Y] = true;

                // Is the room completely filled with water when you enter?
                if (top.Y >= height - top.Times / width)
                {
                    continue;
                }

                // Up
                if (up[top.Y][top.X] != 'x')
                {
                    Node next = new Node
                        {
                            X = top.X,
                            Y = top.Y - 1,
                            Times = top.Times + up[top.Y][top.X] - '0',
                        };
                    priorityQueue.Push(next);
                }

                // Down
                if (down[top.Y][top.X] != 'x')
                {
                    Node next = new Node
                    {
                        X = top.X,
                        Y = top.Y + 1,
                        Times = top.Times + down[top.Y][top.X] - '0',
                    };
                    priorityQueue.Push(next);
                }

                // East
                if (east[top.Y][top.X] != 'x')
                {
                    Node next = new Node
                    {
                        X = top.X + 1,
                        Y = top.Y,
                        Times = top.Times + east[top.Y][top.X] - '0',
                    };
                    priorityQueue.Push(next);
                }

                // West
                if (west[top.Y][top.X] != 'x')
                {
                    Node next = new Node
                    {
                        X = top.X - 1,
                        Y = top.Y,
                        Times = top.Times + west[top.Y][top.X] - '0',
                    };
                    priorityQueue.Push(next);
                }
            }

            return -1;
        }

        private class Node : IComparable
        {
            public int X { get; set; }
            public int Y { get; set; }
            public int Times { get; set; }

            public Node() { }

            public Node(int x, int y, int times)
            {
                X = x;
                Y = y;
                Times = times;
            }

            int IComparable.CompareTo(object obj)
            {
                Node another = (Node)obj;
                if (this.Times > another.Times)
                {
                    return 1;
                }
                else if (this.Times < another.Times)
                {
                    return -1;
                }
                return 0;
            }
        }
    }
}
