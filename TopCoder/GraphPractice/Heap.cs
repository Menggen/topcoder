﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TopCoder.GraphPractice
{
    public abstract class Heap<T> : IEnumerable<T>
    {
        public int Count 
        { 
            get { return tail; } 
        }

        public int Capacity 
        { 
            get { return capacity; } 
        }

        protected Comparer<T> Comparer { get; private set; }

        protected abstract bool IsDominated(T x, T y);

        protected Heap() : this(Comparer<T>.Default) { }

        protected Heap(Comparer<T> comparer) : this(Enumerable.Empty<T>(), comparer) { }

        protected Heap(IEnumerable<T> collection) : this(collection, Comparer<T>.Default) { }

        protected Heap(IEnumerable<T> collection, Comparer<T> comparer)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("Null |collection|");
            }
            if (comparer == null)
            {
                throw new ArgumentNullException("Null |comparer|");
            }

            Comparer = comparer;

            foreach (var item in collection)
            {
                if (Count == Capacity)
                {
                    Grow();
                }
                heap[tail++] = item;
            }

            for (int i = Parent(tail - 1); i >= 0; i--)
            {
                BubbleDown(i);
            }
        }

        public void Push(T item)
        {
            if (Count == Capacity)
            {
                Grow();
            }
            heap[tail++] = item;
            BubbleUp(tail - 1);
        }

        private void BubbleUp(int i)
        {
            if (i == 0 || IsDominated(heap[Parent(i)], heap[i]))
            {
                return; 
            }
            Swap(i, Parent(i));
            BubbleUp(Parent(i));
        }

        public T Peek()
        {
            if (Count == 0)
            {
                throw new InvalidOperationException("Heap is empty");
            }
            return heap[0];
        }

        public T Pop()
        {
            if (Count == 0)
            {
                throw new InvalidOperationException("Heap is empty");
            }
            T ret = heap[0];
            tail--;
            Swap(tail, 0);
            BubbleDown(0);
            return ret;
        }

        private void BubbleDown(int i)
        {
            int dominatingNode = Dominate(i);
            if (dominatingNode == i)
            {
                return;
            }
            Swap(i, dominatingNode);
            BubbleDown(dominatingNode);
        }

        private int Dominate(int i)
        {
            int dominatingNode = i;
            dominatingNode = GetDominating(YoungChild(i), dominatingNode);
            dominatingNode = GetDominating(OldChild(i), dominatingNode);
            return dominatingNode;
        }

        private int GetDominating(int newNode, int dominatingNode)
        {
            if (newNode < tail && !IsDominated(heap[dominatingNode], heap[newNode]))
            {
                return newNode;
            }
            else
            {
                return dominatingNode;
            }
        }

        private void Swap(int i, int j)
        {
            T temp = heap[i];
            heap[i] = heap[j];
            heap[j] = temp;
        }

        private static int Parent(int i)
        {
            return (i + 1) / 2 - 1;
        }

        private static int YoungChild(int i)
        {
            return (i + 1) * 2 - 1;
        }

        private static int OldChild(int i)
        {
            return YoungChild(i) + 1;
        }

        private void Grow()
        {
            int newCapacity = capacity * GrowthFactor + MinGrowth;
            var newHeap = new T[newCapacity];
            Array.Copy(heap, newHeap, capacity);
            heap = newHeap;
            capacity = newCapacity;
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return heap.Take(Count).GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return heap.Take(Count).GetEnumerator();
        }

        private const int InitialCapacity = 0;

        private const int GrowthFactor = 2;
        
        private const int MinGrowth = 1;

        private int capacity = InitialCapacity;
        
        private T[] heap = new T[InitialCapacity];
        
        private int tail = 0;
    }

    public class MaxHeap<T> : Heap<T>
    {
        public MaxHeap() : this(Comparer<T>.Default) { }

        public MaxHeap(Comparer<T> comparer) : base(comparer) { }

        public MaxHeap(IEnumerable<T> collection, Comparer<T> comparer) 
            : base(collection, comparer) { }

        public MaxHeap(IEnumerable<T> collection) : base(collection) { }

        protected override bool IsDominated(T x, T y)
        {
            return Comparer.Compare(x, y) >= 0;
        }
    }

    public class MinHeap<T> : Heap<T>
    {
        public MinHeap() : this(Comparer<T>.Default) { }

        public MinHeap(Comparer<T> comparer) : base(comparer) { }

        public MinHeap(IEnumerable<T> collection) : base(collection) { }

        public MinHeap(IEnumerable<T> collection, Comparer<T> comparer) 
            : base(collection, comparer) { }

        protected override bool IsDominated(T x, T y)
        {
            return Comparer.Compare(x, y) <= 0;
        }
    }
}
