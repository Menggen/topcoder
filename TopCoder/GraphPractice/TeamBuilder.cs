﻿using System;
using System.Collections.Generic;

namespace TopCoder.GraphPractice
{
    public class TeamBuilder
    {
        // Floyd-Warshall
        public int[] specialLocations(String[] paths)
        {
            int N = paths.Length;

            bool[,] adj = new bool[N, N];

            // Init adjacency matrix
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    adj[i, j] = (i == j) ? true : (paths[i][j] == '1');
                }
            }

            // Iterate
            for (int k = 0; k < N; k++)
            {
                for (int i = 0; i < N; i++)
                {
                    for (int j = 0; j < N; j++)
                    {
                        if (!adj[i, j] && (adj[i, k] & adj[k, j]))
                        {
                            adj[i, j] = true;
                        }
                    }
                }
            }

            // Populate results
            int numReachingAll = 0;
            for (int i = 0; i < N; i++)
            {
                int numReaching = 0;
                for (int j = 0; j < N; j++)
                {
                    if (adj[i, j])
                    {
                        numReaching++;
                    }
                }
                if (numReaching == N)
                {
                    numReachingAll++;
                }
            }

            int numReachedByAll = 0;
            for (int j = 0; j < N; j++)
            {
                int numReached = 0;
                for (int i = 0; i < N; i++)
                {
                    if (adj[i, j])
                    {
                        numReached++;
                    }
                }
                if (numReached == N)
                {
                    numReachedByAll++;
                }
            }

            return new int[] { numReachingAll, numReachedByAll };
        }
    }
}
