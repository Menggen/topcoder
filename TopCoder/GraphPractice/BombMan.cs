﻿using System;
using System.Collections.Generic;

namespace TopCoder.GraphPractice
{
    // http://community.topcoder.com/stat?c=problem_statement&pm=2274&rd=5009
    public class BombMan
    {
        public int shortestPath(String[] maze, int bombs)
        {
            int width = maze[0].Length;
            int height = maze.Length;

            // Do not declare bool[,] visited = new bool[width, height].
            // It is wrong!
            bool[, ,] visited = new bool[width, height, bombs + 1];

            Node startNode = GetStartNode(ref maze, bombs);
            
            MinHeap<Node> priorityQueue = new MinHeap<Node>();

            priorityQueue.Push(startNode);

            while (priorityQueue.Count > 0)
            {
                Node top = priorityQueue.Pop();

                if (top.X < 0 || top.X >= width || top.Y < 0 || top.Y >= height)
                {
                    continue;
                }

                if (visited[top.X, top.Y, top.Bombs])
                {
                    continue;
                }
                visited[top.X, top.Y, top.Bombs] = true;

                if (maze[top.Y][top.X] == 'E')
                {
                    return top.Times;
                }

                for (int dx = -1; dx <= 1; dx += 2)
                {
                    Node next = new Node(top.X + dx, top.Y, top.Times, top.Bombs);

                    if (next.X >= 0 && next.X < width && next.Y >= 0 && next.Y < height)
                    {
                        if (maze[next.Y][next.X] == '#')
                        {
                            if (next.Bombs == 0)
                            {
                                continue;
                            }
                            next.Times += 3;
                            next.Bombs--;
                            priorityQueue.Push(next);
                        }
                        else 
                        {
                            next.Times += 1;
                            priorityQueue.Push(next);
                        }
                    }
                }
                for (int dy = -1; dy <= 1; dy += 2)
                {
                    Node next = new Node(top.X, top.Y + dy, top.Times + 1, top.Bombs);

                    if (next.X >= 0 && next.X < width && next.Y >= 0 && next.Y < height)
                    {
                        if (maze[next.Y][next.X] == '#')
                        {
                            next.Times += 2;
                            next.Bombs--;
                        }

                        if (next.Bombs >= 0)
                        {
                            priorityQueue.Push(next);
                        }
                    }
                }
            }

            return -1;
        }

        private Node GetStartNode(ref String[] maze, int bombs)
        { 
            int width = maze[0].Length;
            int height = maze.Length;
            for (int j = 0; j < height; j++)
            {
                for (int i = 0; i < width; i++)
                {
                    if (maze[j][i] == 'B')
                    {
                        return new Node(i, j, 0, bombs);
                    }
                }
            }

            // Should not reach here
            return new Node();
        }

        private class Node : IComparable
        {
            public int X { get; set; }
            public int Y { get; set; }
            public int Times { get; set; }
            public int Bombs { get; set; }
            
            public Node() { }

            public Node(int x, int y, int times, int bombs)
            {
                X = x;
                Y = y;
                Times = times;
                Bombs = bombs;
            }

            int IComparable.CompareTo(object obj)
            {
                Node another = (Node)obj;
                if (this.Times > another.Times)
                {
                    return 1;
                }
                else if (this.Times < another.Times)
                {
                    return -1;
                }
                return 0;
            }
        }
    }
}
