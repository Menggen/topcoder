﻿using System;
using System.Collections.Generic;

namespace TopCoder.GraphPractice
{
    // http://community.topcoder.com/stat?c=problem_statement&pm=1110&rd=4585
    public class PathFinding
    {
        // BFS
        public int minTurns(String[] board)
        {
            int width = board[0].Length;
            int height = board.Length;
            bool[, , ,] visited = new bool[width, height, width, height];

            Node initialNode = GetInitialNode(board);
            Queue<Node> queue = new Queue<Node>();
            
            queue.Enqueue(initialNode);

            while (queue.Count > 0)
            {
                Node node = queue.Dequeue();

                if (!IsValidPosition(ref node, ref board))
                {
                    continue;
                }

                if (ArePlayersOverlapping(ref node))
                {
                    continue;
                }

                if (visited[node.PlayerAX, node.PlayerAY, node.PlayerBX, node.PlayerBY])
                {
                    continue;
                }

                visited[node.PlayerAX, node.PlayerAY, node.PlayerBX, node.PlayerBY] = true;

                if (IsSwitched(ref node, ref initialNode))
                {
                    return node.Steps;
                }

                for (int aDx = -1; aDx <= 1; aDx++)
                {
                    for (int aDy = -1; aDy <= 1; aDy++)
                    {
                        for (int bDx = -1; bDx <= 1; bDx++)
                        {
                            for (int bDy = -1; bDy <= 1; bDy++)
                            { 
                                // Avoid crossing
                                if (node.PlayerAX == node.PlayerBX + bDx &&
                                    node.PlayerAY == node.PlayerBY + bDy &&
                                    node.PlayerBX == node.PlayerAX + aDx &&
                                    node.PlayerBY == node.PlayerAY + aDy)
                                {
                                    continue;
                                }

                                Node next = new Node()
                                {
                                    PlayerAX = node.PlayerAX + aDx,
                                    PlayerAY = node.PlayerAY + aDy,
                                    PlayerBX = node.PlayerBX + bDx,
                                    PlayerBY = node.PlayerBY + bDy,
                                    Steps = node.Steps + 1,
                                };

                                queue.Enqueue(next);
                            }
                        }
                    }
                }
            }

            return -1;
        }

        private Node GetInitialNode(String[] board)
        {
            Node initialNode = new Node();

            int width = board[0].Length;
            int height = board.Length;
            for (int j = 0; j < height; j++)
            {
                for (int i = 0; i < width; i++)
                {
                    if (board[j][i] == 'A')
                    {
                        initialNode.PlayerAX = i;
                        initialNode.PlayerAY = j;
                    }
                    if (board[j][i] == 'B')
                    {
                        initialNode.PlayerBX = i;
                        initialNode.PlayerBY = j;
                    }
                }
            }

            return initialNode;
        }

        private bool IsValidPosition(ref Node node, ref String[] board)
        {
            int width = board[0].Length;
            int height = board.Length;

            // Check if Player A is out of bound.
            if (node.PlayerAX < 0 || 
                node.PlayerAY < 0 || 
                node.PlayerAX >= width || 
                node.PlayerAY >= height)
            {
                return false;
            }

            // Check if Player B is out of bound.
            if (node.PlayerBX < 0 ||
                node.PlayerBY < 0 ||
                node.PlayerBX >= width || 
                node.PlayerBY >= height)
            {
                return false;
            }

            // Check if Player A is on a wall.
            if (board[node.PlayerAY][node.PlayerAX] == 'X')
            {
                return false;
            }

            // Check if Player B is on a wall.
            if (board[node.PlayerBY][node.PlayerBX] == 'X')
            {
                return false;
            }

            return true;
        }

        private bool ArePlayersOverlapping(ref Node node)
        {
            return node.PlayerAX == node.PlayerBX && 
                node.PlayerAY == node.PlayerBY;
        }

        private bool IsSwitched(ref Node node, ref Node initialNode)
        {
            return node.PlayerAX == initialNode.PlayerBX &&
                node.PlayerAY == initialNode.PlayerBY &&
                node.PlayerBX == initialNode.PlayerAX &&
                node.PlayerBY == initialNode.PlayerAY;
        }

        private class Node
        {
            public int PlayerAX { get; set; }
            public int PlayerAY { get; set; }
            public int PlayerBX { get; set; }
            public int PlayerBY { get; set; }
            public int Steps { get; set; }
        }
    }
}
