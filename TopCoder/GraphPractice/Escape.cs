﻿using System;
using System.Collections.Generic;

namespace TopCoder.GraphPractice
{
    // http://community.topcoder.com/stat?c=problem_statement&pm=1170&rd=4371
    public class Escape
    {
        public int lowest(String[] harmful, String[] deadly)
        {
            int[,] board = GetBoard(ref harmful, ref deadly);
            bool[,] visited = new bool[501, 501];
            int[,] lowestDamages = new int[501, 501];

            Node startingNode = new Node(0, 0);
            Queue<Node> queue = new Queue<Node>();

            queue.Enqueue(startingNode);

            while (queue.Count > 0)
            {
                Node node = queue.Dequeue();

                if (node.X < 0 || node.X > 500 || node.Y < 0 || node.Y > 500)
                {
                    continue;
                }

                if (board[node.X, node.Y] == -1)
                {
                    continue;
                }
                
                if (visited[node.X, node.Y])
                {
                    continue;
                }
                
                visited[node.X, node.Y] = true;

                if (node.X == 500 && node.Y == 500)
                {
                    return lowestDamages[500, 500];
                }

                for (int dx = -1; dx <= 1; dx += 2)
                {
                    Node next = new Node(node.X + dx, node.Y);

                    if (!(next.X < 0 || next.X > 500 || next.Y < 0 || next.Y > 500))
                    {
                        lowestDamages[next.X, next.Y] =
                            lowestDamages[node.X, node.Y] + board[next.X, next.Y];

                        queue.Enqueue(next);
                    }
                }
                for (int dy = -1; dy <= 1; dy += 2)
                {
                    Node next = new Node(node.X, node.Y + dy);
                    if (!(next.X < 0 || next.X > 500 || next.Y < 0 || next.Y > 500))
                    {
                        lowestDamages[next.X, next.Y] =
                            lowestDamages[node.X, node.Y] + board[next.X, next.Y];
                        
                        queue.Enqueue(next);
                    }
                }
            }

            return -1;
        }

        private int[,] GetBoard(ref String[] harmful, ref String[] deadly)
        {
            int[,] board = new int[501, 501];
            BuildRegion(ref harmful, ref board, 1);
            BuildRegion(ref deadly, ref board, -1);
            board[0, 0] = 0;
            return board;
        }

        private void BuildRegion(ref String[] regionStyle, ref int[,] board, int damage)
        {
            foreach (String region in regionStyle)
            {
                String[] corners = region.Split();
                int x1 = Int32.Parse(corners[0]);
                int y1 = Int32.Parse(corners[1]);
                int x2 = Int32.Parse(corners[2]);
                int y2 = Int32.Parse(corners[3]);

                if (x1 > x2)
                {
                    int temp = x1;
                    x1 = x2;
                    x2 = temp;
                }
                if (y1 > y2)
                {
                    int temp = y1;
                    y1 = y2;
                    y2 = temp;
                }

                for (int x = x1; x <= x2; x++)
                {
                    for (int y = y1; y <= y2; y++)
                    {
                        board[x, y] = damage;
                    }
                }
            }
        }

        private class Node
        {
            public int X { get; set; }

            public int Y { get; set; }

            public Node()
            { }

            public Node(int x, int y)
            {
                X = x;
                Y = y;
            }
        }
    }
}
