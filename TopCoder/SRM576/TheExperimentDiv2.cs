﻿using System;
using System.Collections.Generic;

namespace TopCoder.SRM576
{
    public class TheExperimentDiv2
    {
        public int[] determineHumidity(int[] intensity, int L, int[] leftEnd)
        {
            const int NeverSeenBefore = -1;

            List<int> seenList = new List<int>();
            for (int i = 0; i < intensity.Length; i++)
            {
                seenList.Add(NeverSeenBefore);
            }
            for (int i = leftEnd.Length - 1; i >= 0; i--)
            {
                for (int j = 0; j < L; j++)
                {
                    seenList[leftEnd[i] + j] = i;
                }
            }

            List<int> humidityList = new List<int>();
            for (int i = 0; i < leftEnd.Length; i++)
            {
                humidityList.Add(0);
            }
            for (int i = 0; i < seenList.Count; i++)
            {
                if (seenList[i] != NeverSeenBefore)
                {
                    humidityList[seenList[i]] += intensity[i];
                }
            }
            return humidityList.ToArray();
        }
    }
}
