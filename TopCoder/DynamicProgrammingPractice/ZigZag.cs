﻿using System;
using System.Collections.Generic;

namespace TopCoder.DynamicProgrammingPractice
{
    // http://community.topcoder.com/stat?c=problem_statement&pm=1259&rd=4493
    public class ZigZag
    {
        public int longestZigZag(int[] sequence)
        {
            int alternatingCount = 0;
            int lastSeen = 0;

            for (int i = 0; i < sequence.Length - 1; i++)
            {
                if (sequence[i] < sequence[i + 1])
                {
                    if (alternatingCount > 0 && lastSeen == 1)
                    {
                        continue;
                    }
                    alternatingCount++;
                    lastSeen = 1;
                }
                else if (sequence[i] > sequence[i + 1])
                {
                    if (alternatingCount > 0 && lastSeen == -1)
                    {
                        continue;
                    }
                    alternatingCount++;
                    lastSeen = -1;
                }
            }

            return alternatingCount + 1;
        }
    }
}
