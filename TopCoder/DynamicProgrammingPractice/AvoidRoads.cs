﻿using System;
using System.Collections.Generic;

namespace TopCoder.DynamicProgrammingPractice
{
    public class AvoidRoads
    {
        // Dynamic programming
        // Tutorial: http://community.topcoder.com/tc?module=Static&d1=features&d2=040104
        // Problem: http://community.topcoder.com/stat?c=problem_statement&pm=1889&rd=4709
        public long numWays(int width, int height, String[] bad)
        {
            HashSet<String> badHash = new HashSet<string>(bad);

            long[,] query = new long[width + 1, height + 1];
            query[0, 0] = 1;

            for (int i = 0; i <= width; i++)
            {
                for (int j = 0; j <= height; j++)
                {
                    if (i == 0 && j == 0)
                    {
                        continue;
                    }

                    long fromLeft = 0;
                    if (i > 0 && query[i - 1, j] > 0) 
                    {
                        String inwardBlock = String.Format("{0} {1} {2} {3}", i - 1, j, i, j);
                        String outwardBlock = String.Format("{0} {1} {2} {3}", i, j, i - 1, j);
                        if (!(badHash.Contains(inwardBlock) || badHash.Contains(outwardBlock)))
                        {
                            fromLeft = query[i - 1, j];    
                        }
                    }

                    long fromBottom = 0;
                    if (j > 0 && query[i, j - 1] > 0)
                    {
                        String inwardBlock = String.Format("{0} {1} {2} {3}", i, j - 1, i, j);
                        String outwardBlock = String.Format("{0} {1} {2} {3}", i, j, i, j - 1);
                        if (!(badHash.Contains(inwardBlock) || badHash.Contains(outwardBlock)))
                        {
                            fromBottom = query[i, j - 1];
                        }
                    }
                    query[i, j] = fromLeft + fromBottom;
                }
            }

            return query[width, height];
        }
    }
}
